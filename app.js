// in sublime
var express = require('express');
var port = process.env.PORT || 3000;
var app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.get('/', function (req, res) {
 res.send(JSON.stringify({ Hello: 'World'}));
});
app.get("/hola", (req,res)=>{
    res.send("NICE!")
})
app.listen(port, function () {
 console.log(`Example app listening on port !`);
});