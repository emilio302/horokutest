// in sublime
const express = require('express');
const fs = require('fs');
const uuidv4 = require('uuid/v4');
var port = process.env.PORT || 3000;
var heroes = [];
var app = express();
const { Client } = require('pg');
const bodyParser = require('body-parser');

const client = new Client({
    connectionString: process.env.DATABASE_URL,
    ssl: true,
  });

app.use(bodyParser.json());
const tokens = {};

app.get("/getToken", (req,res)=>{
    const Hard = uuidv4();
    tokens[Hard] = true;
    setTimeout(()=>{
        tokens[Hard] = false;
    },1000);
    res.jsonp({ Easy: 'EsteEsTuTokenDePasarLaPrimeraPrueba', Hard})
})
app.post("/getFirstReward",(req,res)=>{
    //console.log(req.body.Esto_son);
    switch (true){
        case req.body.Esto_son == "EsteEsTuTokenDePasarLaPrimeraPrueba":
            res.send("Enohrabuena, pasaste la prueba 1");
            console.log(req.get('Heroe'));
            heroes.push(req.get('Heroe'));
            
            break;
        case tokens[req.body.Esto_son]:
            res.send("Enohrabuena, pasaste la prueba en modo dificil!");
            console.log(req.get('Heroe'));
            break;
        default:
            res.sendfile("./badFace.jpg");
    }
})

app.get("/getHeroes", (req,res)=>{
    client.connect();
    client.query('SELECT * FROM optivapostman.heroes;', (err, res) => {
        console.log("Genial");
        if (err) throw err;
        for (let row of res.rows) {
          console.log(JSON.stringify(row));
        }
        res.jsonp(res.rows);
        client.end();
      });
})

app.listen(port, function () {
 console.log(`Example app listening on port !`);
});